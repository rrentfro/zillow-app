<?php
namespace ZillowApp;

/**
* Application class
* 
* The application class represents the main routing and request handling for the app
*/
class Application
{
    /**
    * Handles initial application routing
    */
    public function __construct()
    {
        $request = $this->getUrl();

        switch($request->getController())
        {
            default:
                    require_once(Config::$filePath . 'app/controller/init.php');
                    return new InitController($request);
                break;
            case 'api':
                    require_once(Config::$filePath . 'app/controller/api.php');
                    return new ApiController($request);
                break;    
                    
        }        
    }
    
    /**
    * Retrieves the request URL and parses it into a ApplicationRequest object
    * 
    * @return ApplicationRequest Returns an ApplicationRequest object
    */
    public function getUrl()
    {
        $request = new ApplicationRequest();
        $parmList = array();
        if (isset($_GET['url'])) {

            //Split URL parts and populate the ApplicationRequest object
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_STRING);
            $url = explode('/', $url);

            $controller = (isset($url[0]) ? urldecode($url[0]) : null);
            $request->setController($controller);
            
            $action = (isset($url[1]) ? urldecode($url[1]) : null);
            $request->setAction($action);
            
            $parmList[1] = (isset($url[2]) ? urldecode($url[2]) : null);
            $parmList[2] = (isset($url[3]) ? urldecode($url[3]) : null);
            $parmList[3] = (isset($url[4]) ? urldecode($url[4]) : null);
            $parmList[4] = (isset($url[5]) ? urldecode($url[5]) : null);
            $parmList[5] = (isset($url[6]) ? urldecode($url[6]) : null);
            $parmList[6] = (isset($url[7]) ? urldecode($url[7]) : null);
            $parmList[7] = (isset($url[8]) ? urldecode($url[8]) : null);
            $parmList[8] = (isset($url[9]) ? urldecode($url[9]) : null);
            $parmList[9] = (isset($url[10]) ? urldecode($url[10]) : null);
            
            $request->setParam($parmList);
        }
        return $request;
    }
}

/**
* ApplicationRequest class
* 
* This class is a container for the request for the application
*/
class ApplicationRequest
{
    /**
    * @var string $controller The controller to be used
    */
    private $controller = null;
    
    /**
    * @var string $action The action to be taken by the controller
    */
    private $action = null;
    
    /**
    * @var array $param The parameters for the action called on the controller
    */
    private $param = array();

    /**
    * Setter method for controller
    * 
    * @param string $controller The controller to be set
    * @return boolean Returns true on completion
    */
    public function setController($controller)
    {
        $this->controller = $controller;
        return true;         
    }
    
    /**
    * Getter method for controller
    * 
    * @return mixed Returns the current set controller for the request
    */
    public function getController()
    {
        return $this->controller;    
    }
    
    /**
    * Setter method for action
    * 
    * @param string $action The action to be set
    * @return boolean Returns true on completion
    */
    public function setAction($action)
    {
        $this->action = $action;    
    }
    
    /**
    * Getter method for action
    * 
    * @return mixed Returns the current set action for the request
    */
    public function getAction()
    {
        return $this->action;    
    }
    
    /**
    * Setter method for param
    * 
    * @param array $param The array of parameters to be set
    * @return boolean Returns true on completion
    */
    public function setParam($param)
    {
        $this->param = $param;    
    }
    
    /**
    * Getter method for param
    * 
    * @return array Returns the current set params for the request
    */
    public function getParams()
    {
        return $this->param;    
    }
    
    /**
    * Getter method for specific param
    * 
    * @return mixed Returns the value for the request value or null when no value is found
    */
    public function getParam($positionId)
    {
        if (isset($this->param[$positionId])) return $this->param[$positionId];
        return null;    
    }
}
?>