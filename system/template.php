<?php
namespace ZillowApp;

/**
* Template class
* 
* This is a simple template class to process templates and template values for display
*/

class Template
{
    /**
    * @var string $template The loaded template content
    */
    private $template;
    
    /**
    * Loads the template class during object instantiation
    * 
    * @param string $template The filename of the template view to be loaded
    */
    public function __construct($template)
    {
        $templateFile = Config::$filePath . 'app/view/' . $template . '.html';
        $handle = fopen($templateFile, "r");
        if ($handle)
        {
            $this->template = fread($handle, filesize($templateFile));    
        } else {
            throw new \Exception('Invalid Template File Read');
        }
    }
    
    /**
    * Sets a template value for processing
    * 
    * @param string $name The name of the value to be located
    * @param string $value The value to be swapped for the located value
    * @return boolean Returns true on completion
    */
    public function setValue($name, $value)
    {
        $this->template = str_replace('{% $' . $name . ' %}', $value, $this->template);
        return true;    
    }
    
    /**
    * Renders view templates to the system
    * 
    * We provide a path at runtime so the application can use fully qualified paths to access resources via .htaccess rules
    * 
    * @param string $webPath The absolute path to be used by the templates content
    * @param boolean $final A flag that controls if rendering should end execution of the application
    */
    public function render($webPath, $final = true)
    {
        $this->setValue('webPath', $webPath);
        print $this->template;
        if ($final === true) die(); //this ends execution if no additional rendering is required  
    }    
}
?>