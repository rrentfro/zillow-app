$(document).ready(function() {

    var mapLat = 0;
    var mapLong = 0;
    
    $('#viewProperty').on('shown.bs.modal', function () {
        refreshMap(mapLat, mapLong);
    });
   
    $( "#searchButton" ).click(function() {
        var map;
        var address = $( "#searchAddress" ).val();
        address = encodeURIComponent(address);
        
        console.log("api/searchZillow/" + address);
    
        $.get("api/searchZillow/" + address, function(data, status){

            //lets see if our result isn't false
            if (typeof data.result !== 'undefined' && data.result !== false) {
                $("#searchResultInfo").html('Search and Find Your Next Home');
                console.log(data.result);
                
                mapLat = data.result.latitude[0];
                mapLong = data.result.longitude[0];
                
                refreshMap(mapLat, mapLong);
                
                var propertyAddress = data.result.street[0] + "<br>" + data.result.city[0] + ", " + data.result.state[0] + " " + data.result.zipcode[0];    
                $("#propertyAddress").html(propertyAddress);   
                $("#propertyLastUpdated").html(data.result.lastUpdated[0]);

                var propertyZestimateAmount = data.result.amount[0];
                propertyZestimateAmount = accounting.formatMoney(propertyZestimateAmount); 
                $("#propertyAmount").html(propertyZestimateAmount);
                
                var valuationHigh = data.result.valuationRange.high;
                var valuationLow = data.result.valuationRange.low;
                valuationHigh = accounting.formatMoney(valuationHigh);
                valuationLow = accounting.formatMoney(valuationLow);
                
                $("#propertyValuation").html('Low: ' + valuationLow + " High: " +  valuationHigh);
                
                var valueChange = data.result.valueChange[0];
                valueChange = accounting.formatMoney(valueChange);
                $("#propertyValueChange").html(valueChange);
                
                var percentile = data.result.percentile[0]; 
                $("#propertyPercentile").html(percentile);

                //display the modal property view
                $('#viewProperty').modal({
                  keyboard: true
                }) 
                
            } else {
                //no result was found
                $("#searchResultInfo").html('0 Search Results Found');
                return false;
            }
        });
    });

    function refreshMap(refreshLat, refreshLong)
    {
        //console.log('mapLat: ' + refreshLat + ' mapLong: ' + refreshLong);
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: parseFloat(refreshLat), lng: parseFloat(refreshLong)},
          zoom: 16
        });
        var currentCenter = map.getCenter();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(currentCenter);
        
        var myLatLng = {lat: parseFloat(mapLat), lng: parseFloat(mapLong)};
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Property'
        });
        
    }
});