<?php
namespace ZillowApp;
/*
* This is the default application entry point
*/

/*
* Load traits used by app
*/
require_once('app/trait/singleton.php');

/*
* Load config for application and init instance
*/
require_once('app/config/config.php');
Config::getInstance();

/*
* Load application bootstrap
*/
require_once(Config::$filePath . 'app/bootstrap.php');

//example basic template processing via PHP for implementing into object
/*
$templateFile = 'app/view/container.html';
$handle = fopen($templateFile, "r");
$template = fread($handle, filesize($templateFile));
$template = str_replace('{% $WebApp %}', Config::$zillowAPIKey, $template);
$template = str_replace('{% $appName %}', Config::$appName, $template);
*/

/*
* Load application
*/
$application = new Application();

  

?>
