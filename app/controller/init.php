<?php
namespace ZillowApp;

/**
* InitController class
* 
* Provides the application init and displays the application interface 
*/
class InitController
{
    /**
    * @var Template $template The template class used by the controller
    */
    private $template;
    
    /**
    * The router for the InitController class
    * 
    * @param ApplicationRequest $request The request being handled by the application
    */
    public function __construct(ApplicationRequest $request)
    {
        /*
        * Require and instantiate the simple template class
        */
        require_once(Config::$filePath . 'system/template.php');
        $this->template = new Template('container');
        
        /*
        * Route the action - in the case of this app we'll only have one
        */
        switch($request->getAction())
        {
            default:
                    $this->init();
                break; 
        }    
    }
    
    /**
    * The init for the application interface - once loaded everything else occurs through ajax and API
    * 
    */
    private function init()
    {
        $this->template->setValue('appName', Config::$appName);
        $this->template->render(Config::$webPath);        
    }
}
?>