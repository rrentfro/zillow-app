<?php
namespace ZillowApp;

/**
* ApiController class
* 
* Provides a basic web service for teh web application
*/
class ApiController
{
    /**
    * The router for the ApiController class
    * 
    * @param ApplicationRequest $request The ApplicationRequest object to be processed
    */
    public function __construct(ApplicationRequest $request)
    {
        switch($request->getAction())
        {
            default:
                    //return invalid request headers
                break;
            case 'searchZillow':
                    $address = $request->getParam(1);
                    $this->searchZillow($address);
                break; 
        }    
    }
    
    /**
    * This method searchs google for geocode and address information then searchs zillow for that located geo information.
    * 
    * Provides results in JSON format
    * 
    * @param string $address The address to be searched for
    * @return boolean Returns true when result is found, false if no result is found
    */
    private function searchZillow($address)
    {
        $address = urldecode($address);
        $addressFormat = null;
        $cityStateZipFormat = null;
        $zipCodeFlag = false;
        
        $zillowServiceURL = 'http://www.zillow.com/webservice/GetSearchResults.htm';
        $googleServiceURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=AIzaSyB2eCWDOhbMNvtBQne0Z5ACoPQl0AYgVJM';

        $request = file_get_contents($googleServiceURL);
        $json = json_decode($request, true);
        
        //check if results were found by google for the address search
        if (!isset($json['results'][0]['address_components'])) 
        {
            $this->returnJSON(false);
            return false;  
        }
        
        /*
        * These are the components of our response for the google geocode service
        */
        $components = $json['results'][0]['address_components'];
        
        /*
        * Let's prepare the address via geolocation services to ensure we allow for a single line address search
        * Zillow's API only allows for a US address atm, so this works until some sort of international change would be required, then adress to geolocation data would make better sense
        */
        foreach($components as $component)
        {
            if (isset($component['types']) && count($component['types']) > 0)
            {
                switch($component['types'][0])
                {
                    default:
                            continue;
                        break;
                    case 'street_number':
                            $addressFormat = $component['short_name'] . $addressFormat;    
                        break;
                    case 'route':
                            $addressFormat = $addressFormat . $component['short_name'];     
                        break;
                    case 'locality':
                            if ($zipCodeFlag === false) $cityStateZipFormat = $component['short_name'] . ' ' .$cityStateZipFormat;     
                        break;
                    case 'administrative_area_level_1':
                            if ($zipCodeFlag === false) $cityStateZipFormat = $cityStateZipFormat . ' ' .$component['short_name'];    
                        break;
                    case 'postal_code':
                            $cityStateZipFormat = $component['short_name'];
                            $zipCodeFlag = true;    
                        break;
                }    
            }    
        }

        $addressFormat = str_replace(' ', ' ', $addressFormat); //eliminate any possible double spacing
        
        //build the basic request for the zillow api
        $serviceRequest = array(
            'zws-id' => 'zws-id=' . Config::$zillowAPIKey,
            'address' => 'address=' . urlencode($addressFormat),
            'citystatezip' => 'citystatezip=' . $cityStateZipFormat,
        );
        
        $zillowRequestURL = $zillowServiceURL . '?' . implode('&', $serviceRequest);
        $zillowResult = file_get_contents($zillowRequestURL);
        $xml = simplexml_load_string($zillowResult);
        
        //validate the element returned is a valid SimpleXMLElement and that our returned message code is success
        if (!is_a($xml,'SimpleXMLElement') || !isset($xml->message->code) || $xml->message->code > 0)
        {
            $this->returnJSON(false);
            return false; 
        }
        
        $address = $xml->response->results->result->address;
        $zestimate = $xml->response->results->result->zestimate;  

        //prepare our payload
        $propertyInfo = array(
            'street' => $address->street,
            'zipcode' => $address->zipcode,
            'city' => $address->city,
            'state' => $address->state,
            'latitude' => $address->latitude,
            'longitude' => $address->longitude,
            'amount' => $zestimate->amount,
            'lastUpdated' => $zestimate->{'last-updated'},
            'valueChange' => $zestimate->valueChange,
            'valuationRange' => $zestimate->valuationRange,
            'percentile' => $zestimate->percentile,
        );
        
        //return the details as JSON
        $this->returnJSON($propertyInfo);
        return true;         
    }
    
    /**
    * Method used to return data in JSON format
    * 
    * @param mixed $value The value to be transformed into JSON 
    */
    private function returnJSON($value)
    {
        $value = json_encode(array('result' => $value));
        header("Content-type: application/json");
        print $value;
        return true;  
    }    
}

?>