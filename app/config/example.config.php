<?php
namespace ZillowApp;
/**
 * Class Config
 *
 *  The base config class for the example Zillow App
 */
class Config
{
    use Singleton;
    
    /**
    * @var string $webPath The root webpath for the application
    */
    public static $webPath =  'http://localhost/zillow-app/';
    
    /**
    * @var string $filePath The root filepath for the application
    */
    public static $filePath = '';
    
    /**
    * @var string $appName The name what will be used for the app  
    */
    public static $appName = 'Zillow Demo App';
    
    /**
    * @var string $zillowAPIKey The provided zillow API Key 
    */
    public static $zillowAPIKey = 'X1-ZWz1dyb53fdhjf_6jziz';
        
    /**
    * @var string $zillowAPIKey The provided zillow API Key 
    */
    public static $googleAPIKey = 'AIzaSyB2eCWDOhbMNvtBQne0Z5ACoPQl0AYgVJM';

    /**
    * The classes init function
    */
    protected function init()
    {
        
    }
}
?>
